$("table#rasci td:not(:first-of-type):not(:last-of-type)").click(function (e) {
    console.log("spawning" + e.pageX + " " + e.pageY);
    let elem = $('<div class="task-role-info"><div class="card-header">Responsible</div><div class="card-body">Samwise Gamgee</div></div>');
    elem.css({"position": "absolute", "left": ""+e.pageX+"px", "top": ""+e.pageY+"px"});
    elem.click(function (e) {
        $(this).remove();
    });
   $("body").append(elem);
});

// unusbed, can't figure out correct event handler
function set_rasci_cell_height() {
    let max = 0;
    $("table#rasci td").css("height", "");
    $("table#rasci td").each(function () {
        if($(this).height() > max) {
            console.log(max);
            max = $(this).height();
        }
    });
    $("table#rasci td").css("height", ""+max+"px");
}