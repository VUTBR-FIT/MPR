<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <title>@yield('title')</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="/css/ready.css">
    <link rel="stylesheet" href="/css/custom.css">
</head>
<body>
<div class="wrapper">
    <div class="main-header">
        <div class="logo-header">
            <a href="{{ url('home')}}" class="logo">
                Unicorn IS
            </a>
            <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-controls="sidebar" aria-expanded="false" aria-label="Toggle navigation">
            	<span class="navbar-toggler-icon"></span>
         	</button>
        </div>
        <nav class="navbar navbar-header navbar-expand-lg">
            <div class="container-fluid">

                <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false"> <span>{{ Auth::user()->name }}</span> </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <div class="user-box">
                                    <div class="u-text">
                                        <h4>{{ Auth::user()->name }}</h4>
                                        <p class="text-muted">{{ Auth::user()->systemRole->name }}</p></div>
                                </div>
                            </li>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#"><i class="ti-user"></i> My Profile</a>
                            <a class="dropdown-item" href="#"><i class="ti-settings"></i> Account Setting</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item"  href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}<i class="fa fa-power-off"></i></a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <nav class="navbar navbar-header navbar-expand-lg">
            <div class="container-fluid">

            </div>
        </nav>
    </div>
    <div class="sidebar">
        <div class="scrollbar-inner sidebar-wrapper">
            <div class="user">
                <div class="info">
                    <a class="" data-toggle="collapse" href="">
								@if (isset($projectId))<span>
									<span class="rainbow-red">Project Name</span>
                                                                        <!-- @TODO fix this styles -->
                                                                        <span class="active-project"><a href="{{ url("project-list/project-detail/$project->project_id") }}">{{ $project->name }}</a></span>
								</span>
                                                                @endIf
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>
            <ul class="nav">
                <li class="nav-item {{ Request::is('home*') ? 'nav-item active rainbow-orange' : 'nav-item rainbow-orange'}} rainbow-orange">
                    <a href="{{ url('home')}}">
                        <i class="la la-clipboard"></i>
                        <p>Overview</p>
                    </a>
                </li>
                @if (isset($projectId))
                <li class="nav-item {{ Request::is('rasci*') ? 'nav-item active rainbow-yellow' : 'nav-item rainbow-orange'}} rainbow-yellow">
                    <a href="{{ url("rasci/$projectId")}}">
                        <i class="la la-table"></i>
                        <p>RASCI</p>
                    </a>
                </li>
                @endIf
                <li class="separator"></li>
                <li class="nav-item {{ Request::is('project-list*') ? 'nav-item active rainbow-green' : 'nav-item rainbow-green'}} rainbow-green">
                    <a href="{{ url('project-list')}}">
                        <i class="la la-list-alt"></i>
                        <p>Project List</p>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('NSProle-list*') ? 'nav-item active rainbow-blue' : 'nav-item rainbow-blue'}} rainbow-blue">
                    <a href="{{ url('NSProle-list')}}">
                        <i class="la la-object-group"></i>
                        <p>Role List</p>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('user-list*') ? 'nav-item active rainbow-purple' : 'nav-item rainbow-purple'}} rainbow-purple">
                    <a href="{{ url('user-list')}}">
                        <i class="la la-user"></i>
                        <p>User List</p>
                    </a>
                </li>
                <li class="nav-item {{ Request::is('human-resources*') ? 'nav-item active rainbow-pink' : 'nav-item rainbow-pink'}} rainbow-pink">
                    <a href="{{ url('human-resources')}}">
                        <i class="la la-users"></i>
                        <p>Human Resources</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">
                
              @yield('content')

            </div>
        </div>
    </div>
</div>
</body>
<script src="/js/core/jquery.3.2.1.min.js"></script>
<script src="/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="/js/core/popper.min.js"></script>
<script src="/js/core/bootstrap.min.js"></script>
<script src="/js/plugin/chartist/chartist.min.js"></script>
<script src="/js/plugin/chartist/plugin/chartist-plugin-tooltip.min.js"></script>
<script src="/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js"></script>
<script src="/js/plugin/jquery-mapael/jquery.mapael.min.js"></script>
<script src="/js/plugin/jquery-mapael/maps/world_countries.min.js"></script>
<script src="/js/plugin/chart-circle/circles.min.js"></script>
<script src="/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script src="/js/ready.min.js"></script>
</html>
