@extends('layouts.unicornLayout')

@section('title')
  HomePage
@endsection

@section('content')
<div class="col-md-12">
    @include('layouts.status')
    @include('layouts.formErrors')
    <div class="card">
      <div class="card-header">
        <div class="card-title">Human resources</div>
      </div>

        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name ▿</th>
                    <th scope="col">Salary ▿</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                  <tbody>
                        @foreach ($humanResources as $humanResource)
                        <tr>
                          <td>{{ $humanResource->id }}</td>
                          <td>{{ $humanResource->name . " " . $humanResource->surname }}</td>
                          <td>{{ $humanResource->salary }}</td>
                          <td>
                            <a class="btn btn-default btn-sm" href="{{ url("human-resources/edit-human-resource/$humanResource->id") }}">
                              <i class="la la-edit"></i> Edit
                            </a>
                          </td>
                          <td>
                            <a class="btn btn-danger btn-sm"  href="{{ url("human-resources/remove-human-resource/$humanResource->id") }}">
                                <i class="la la-remove"></i> Remove
                            </a>
                          </td>
                        </tr>
                        @endforeach
                  </tbody>
            </table>
            <div class="card-action">
                @if (Auth::user()->systemRole_id != App\User::Guest)
                <a class="btn btn-default" href="{{ url('human-resources/add-human-resource') }}">Add human resource</a>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection
