@extends('layouts.unicornLayout')

@section('title')
  Add human resource
@endsection

@section('content')
<!-- <h4 class="page-title">User List</h4> -->
<div class="row">
  <div class="col-md-12">

    @include('layouts.formErrors')

    <div class="card">
      <div class="card-header">
        <div class="card-title">Add human resource</div>
      </div>

      <form action="{{ url("human-resources/add-human-resource") }}" method="POST">
        {{ csrf_field() }}
        <div class="card-body">

          <div class="form-group">
            <label for="password">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="name" value="{{ old('name') }}" name="name" placeholder="Name" autofocus>
          </div>

          <div class="form-group">
            <label for="password">Surname<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="password" value="{{ old('surname') }}" name="surname" placeholder="Surname">
          </div>

          <div class="form-group">
            <label for="password">Age</label>
            <input type="text" class="form-control" id="age" value="{{ old('age') }}" name="age" placeholder="Age">
          </div>

          <div class="form-group">
            <label for="password">Telephone number</label>
            <input type="text" class="form-control" id="tel" value="{{ old('tel') }}" name="tel" placeholder="Telephone Number">
          </div>

          <div class="form-group">
            <label for="password">Salary<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="salary" value="{{ old('salary') }}" name="salary" placeholder="Salary">
          </div>
        </div>

          <div class="card-action">
            <button  type="submit" class="btn btn-success">Submit</button>
            <a class="btn btn-danger" href="{{ url("human-resources") }}">Cancel</a>
          </div>
      </div>
    </div>
  </div>
@endsection
