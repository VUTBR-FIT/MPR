@extends('layouts.unicornLayout')

@section('title')
  Role detail
@endsection

@section('content')
<div class="container-fluid">
    @include('layouts.status') 
    <h4 class="page-title">{{ $NSProle->name }}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><div class="card-title">Assigned Users</div></div>
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Username</th>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @if ($assignedUsers->count() > 0)
                            @foreach ($assignedUsers as $assignedUser)
                            <tr>
                                <td>{{ $assignedUser->id }}</td>
                                <td>{{ $assignedUser->name }} {{ $assignedUser->surname }}</td>
                                <td class="remove" id="{{ $assignedUser->id }}"><a data-toggle="modal" data-target="#remove-user"><i class="la la-remove"></i></a></td>
                            </tr>
                            @endforeach
                        @else
                             <tr>
                                <td>Nobody is assigned to this NSP role.</td>
                            </tr> 
                        @endif
                        </tbody>
                    </table>
                    <!--div class="card-action">
                        <button class="btn btn-default"  data-toggle="modal" data-target="#add-user-competency">Assign User</button>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-attr" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Attribute</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="attribute-name">Attribute Name</label>
                    <input type="text" class="form-control" id="attribute-name" placeholder="Enter atrribute text">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="add-competency" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Competency</h4>
            </div>
            <div class="form-group">
                <select class="form-control" id="competency">
                    <option>Competency 1</option>
                    <option>Competency 2</option>
                    <option>Competency 3</option>
                    <option>Competency 4</option>
                    <option>Competency 5</option>
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">Add</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="add-user-competency" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Competent User</h4>
            </div>
            <form action="{{ url("NSProle-list/NSProle-detail/NSProleAssignUser") }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="username">User</label>
                        <input type="hidden" class="form-control" id="NSProleId" value="{{ $NSProle->id }}" name="NSProleId" placeholder="NSProleId">
                        <select class="form-control" name="username">
                            @foreach ($users as $user)
                                <option value={{ $user->id }}> {{ $user->name }} {{ $user->surname }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Assign</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="modal fade" id="remove-attr" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete attribute?</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this attribute?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Remove</button>
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="remove-competency" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete competency?</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this competency?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Remove</button>
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="remove-user" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Remove user?</h4>
            </div>
            <form action="{{ url("NSProle-list/NSProle-detail/removeNSProleFromUser") }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="NSProleId" value="{{ $NSProle->id }}" name="NSProleId" placeholder="NSProleId">
                    <input type="hidden" class="form-control" id="removeUser_id" value="" name="removeUser_id" placeholder="user_id">
                    Are you sure you want to remove this user?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Remove</button>
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script>
    $(document).on("click", ".remove", function(e) {
        var currentUserId = e.currentTarget.id
        $(".modal-body #removeUser_id").val( currentUserId );        
    })
</script>

@endsection
