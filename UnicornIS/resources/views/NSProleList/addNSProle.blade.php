@extends('layouts.unicornLayout')

@section('title')
  Add NSP role
@endsection

@section('content')
<!-- <h4 class="page-title">User List</h4> -->
<div class="row">
  <div class="col-md-12">

    @include('layouts.status') 
    @include('layouts.formErrors')

    <div class="card">
      <div class="card-header">
        <div class="card-title">Add NSP role</div>
      </div>

      <form action="{{ url('NSProle-list/add-NSProle') }}" method="POST">
        {{ csrf_field() }}
        <div class="card-body">

          <div class="form-group">
            <label for="Name">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="password" value="" name="name" placeholder="Name" autofocus>
          </div>

          <div class="form-group">
            <label for="Description">Description<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="description" value="" name="description" placeholder="Description">
          </div>
          
          <div class="form-group">
            <label for="Qualification">Qualification</label>
            <input type="text" class="form-control" id="qualification" value="" name="qualification" placeholder="Qualification">
          </div>

          <div class="form-group">
            <label for="Salary">Salary<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="Salary" value="" name="salary" placeholder="Salary">
          </div>

          <div class="card-action">
            <button  type="submit" class="btn btn-success">Submit</button>
            <a class="btn btn-danger" href="{{ url("NSProle-list") }}">Cancel</a>
          </div>
      </div>
    </div>
  </div>
@endsection
