@extends('layouts.unicornLayout')

@section('title')
  Edit NSP Role
@endsection

@section('content')
<!-- <h4 class="page-title">User List</h4> -->
<div class="row">
  <div class="col-md-12">
    @include('layouts.status') 
    @include('layouts.formErrors')

    <div class="card">
      <div class="card-header">
        <div class="card-title">Edit NSP Role</div>
      </div>

      <form action="{{ url("NSProle-list/edit-NSProle/$NSProle->id") }}" method="POST">
        {{ csrf_field() }}
        <div class="card-body">

          <div class="form-group">
            <label for="password">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="password" value="{{ $NSProle->name }}" name="name" placeholder="Name" autofocus>
          </div>

          <div class="form-group">
            <label for="password">Description</label>
            <input type="text" class="form-control" id="password" value="{{ $NSProle->description }}" name="description" placeholder="Description">
          </div>

          <div class="form-group">
            <label for="email">Qualification<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="qualification" value="{{ $NSProle->qualification }}" name="qualification" placeholder="Qualification">
          </div>

          <div class="form-group">
            <label for="password">Salary<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="salary" value="{{ $NSProle->salary }}" name="salary" placeholder="Salary">
          </div>
        </div>

          <div class="card-action">
            <button  type="submit" class="btn btn-success">Edit</button>
            <a class="btn btn-danger" href="{{ url("NSProle-list") }}">Cancel</a>
          </div>
      </div>
    </div>
  </div>
@endsection
