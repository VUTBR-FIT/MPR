@extends('layouts.unicornLayout')
@section('title')
  HomePage
@endsection

@section('content')
<div class="col-md-12">
  @include('layouts.status') 
    <div class="card">
      <div class="card-header">
        <div class="card-title">Role List</div>
      </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Role Name ▿</th>
                    <th scope="col">Salary ▵</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($NSProles as $NSProle)
                    <tr>
                      <td>{{ $NSProle->id }}</td>
                      <td>{{ $NSProle->name }}</td>
                      <td>{{ $NSProle->salary }}</td>
                      <td>
                        <a class="btn btn-default btn-sm" href="{{ url("NSProle-list/edit-NSProle/$NSProle->id") }}">
                          <i class="la la-edit"></i> Edit
                        </a>
                      </td>
                      <td>
                        <a class="btn btn-danger btn-sm"  href="{{ url("NSProle-list/remove-NSProle/$NSProle->id") }}">
                            <i class="la la-remove"></i> Remove
                        </a>
                      </td>
                      <td>
                        <a class="btn btn-default btn-sm"  href="{{ url("NSProle-list/NSProle-detail/$NSProle->id") }}">
                            Detail
                        </a>
                      </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-action">
                @if (Auth::user()->systemRole_id != App\User::Guest)
                <a class="btn btn-default" href="{{ url('NSProle-list/add-NSProle') }}">Add NSP role</a>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
