@extends('layouts.unicornLayout')

@section('title')
  HomePage
@endsection

@section('content')
<div class="col-md-12">
    @include('layouts.status')
    <div class="card">
      <div class="card-header">
        <div class="card-title">User List</div>
      </div>

        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Username ▿</th>
                    <th scope="col">Roles ▿</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                  <tbody>
                        @foreach ($users as $user)
                        <tr>
                          <td>{{ $user->id }}</td>
                          <td>{{ $user->name . " " . $user->surname }}</td>
                          <td>{{ $user->systemRole->name }}</td>
                          <td>
                            <a class="btn btn-default btn-sm" href="{{ url("user-list/edit-user/$user->id") }}">
                              <i class="la la-edit"></i> Edit
                            </a>
                          </td>
                          <td>
                            <a class="btn btn-danger btn-sm"  href="{{ url("user-list/remove-user/$user->id") }}">
                                <i class="la la-remove"></i> Remove
                            </a>
                          </td>
                        </tr>
                        @endforeach
                  </tbody>
            </table>
            <div class="card-action">
                @if (Auth::user()->systemRole_id == App\User::CEO)
                <a class="btn btn-default" href="{{ url('user-list/add-user') }}">Add User</a>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@endsection
