@extends('layouts.unicornLayout')

@section('title')
  Edit user
@endsection

@section('content')
<!-- <h4 class="page-title">User List</h4> -->
<div class="row">
  <div class="col-md-12">
    @include('layouts.status') 
    @include('layouts.formErrors')

    <div class="card">
      <div class="card-header">
        <div class="card-title">Edit user</div>
      </div>

      <form action="{{ url("user-list/edit-user/$user->id") }}" method="POST">
        {{ csrf_field() }}
        <div class="card-body">

          <div class="form-group">
            <label for="password">Name<span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="password" value="{{ $user->name }}" name="name" placeholder="Name" autofocus>
          </div>

          <div class="form-group">
            <label for="password">Surname</label>
            <input type="text" class="form-control" id="password" value="{{ $user->surname }}" name="surname" placeholder="Surname">
          </div>

          <div class="form-group">
            <label for="exampleFormControlSelect1">Role<span class="text-danger">*</span></label>
            @if (Auth::user()->systemRole_id == 2)
            <select class="form-control" id="exampleFormControlSelect1" name="role">
            @else
            <select class="form-control" id="exampleFormControlSelect1" name="role" disabled>
            @endif
              @foreach ($roles as $role)
                <option value="{{ $role->id }}" {{ $role->id == $user->systemRole_id ? "selected=\"selected\"" : '' }}>{{ $role->name }}</option>
               @endforeach
            </select>
          </div>

          <div class="form-group">
            <label for="email">Email Address<span class="text-danger">*</span></label>
            <input type="email" class="form-control" id="email" value="{{ $user->email }}" name="email" placeholder="Enter Email" disabled>
          </div>

          {{-- <div class="form-group">
            <label for="password">Password<span class="text-danger">*</span></label>
            <input type="password" class="form-control" id="password" value="{{ encrypt($user->password) }}" name="password" placeholder="Password">
          </div> --}}
          </div>

          <div class="card-action">
            <button  type="submit" class="btn btn-success">Edit</button>
            <a class="btn btn-danger" href="{{ url("user-list") }}">Cancel</a>
          </div>
      </div>
    </div>
  </div>
@endsection
