@extends('layouts.unicornLayout')

@section('title')
RASCI
@endsection

@section('content')
<h4 class="page-title">RASCI Matrix</h4>
<div class="col-md-12">
    @include('layouts.status')
    @include('layouts.formErrors')
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered table-hover" id="rasci">
                <thead>
                    <tr>
                        <th class="edge"></th>
                        @foreach ($roles as $role)
                        <th>{{ $role->name }}</th>
                        @endforeach
                        <td style="width: 35px"></td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tasks as $rowId => $task)
                    <tr>
                        <td>{{ $task->name }}</td>
                        @foreach ($roles as $role)
                            @if (array_key_exists($role->name, $cells[$rowId]))
                        <td class="rasci-{{ strtolower($cells[$rowId][$role->name]->value) }}" data-toggle="modal" data-target="#rasci-assign-user-{{ $cells[$rowId][$role->name]->id }}">
                            {{ $cells[$rowId][$role->name]->value }}
                        </td>
                            @else
                        <td></td>
                            @endif
                        @endforeach
                        <td>
                            <ul class="edit-delete">
                                <li>
                                    <a data-toggle="modal" data-target="#edit-task-{{ $task->id }}"><i class="la la-edit"></i></a>
                                </li>
                                <li class="separator"></li>
                                <li>
                                    <a data-toggle="modal" data-target="#remove-task-{{ $task->id }}"><i class="la la-remove"></i></a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-action">
                <button class="btn btn-default" data-toggle="modal" data-target="#add-task">Add Task</button>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="add-task" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Task</h4>
            </div>
            <form name="add-task" action="{{ url("rasci/$projectId/add-task") }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="task-name">Task Name</label>
                        <input type="text" class="form-control" id="task-name" name="name" placeholder="Enter task name">
                    </div>
                    @foreach ($roles as $role)
                    <div class="form-group">
                        <label for="{{ $role->id }}">{{ $role->name }}</label>
                        <select class="form-control" id="{{ $role->id }}" name="roles[{{ $role->id }}]">
                            <option></option>
                            @foreach ($responsibility as $resp)
                            <option value="{{ $resp->id }}">{{ $resp->value }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
@foreach ($tasks as $rowId => $task)
<div class="modal fade" id="edit-task-{{ $task->id }}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Task - {{ $task->name }}</h4>
            </div>
            <form action="{{ url("rasci/$projectId/update-task/$task->id") }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="task-name">Task Name</label>
                        <input type="text" class="form-control" id="edit-task-name" name="name" placeholder="Enter task name" value="{{ $task->name }}">
                    </div>
                    @foreach ($roles as $role)
                    <div class="form-group">
                        <label for="{{ $role->id }}">{{ $role->name }}</label>
                        <select class="form-control" id="{{ $role->id }}" name="roles[{{ $role->id }}]">
                            <option></option>
                            @foreach ($responsibility as $resp)
                            <option value="{{ $resp->id }}" 
                                @if (array_key_exists($role->name, $cells[$rowId]))
                                    @if ($resp->id == $cells[$rowId][$role->name]->responsibility_Id)
                                selected
                                    @endif  
                                @endif>{{ $resp->value }}</option>
                            @endforeach
                        </select>
                    </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>
</div>
    @foreach ($cells[$rowId] as $cell)
<div class="modal fade" id="rasci-assign-user-{{ $cell->id }}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="{{ url("rasci/$projectId/update-task/$task->id/role/$cell->NSP_id") }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user-picker">{{ $cell->value }} Person</label>
                        <select class="form-control" id="user-picker" name="person">
                            <option></option>
                            @foreach ($users[$cell->NSP_id] as $user)
                            <option value="{{ $user->id }}" 
                                @if ($user->id == $cell->humanResource_id) 
                                selected
                                @endif>{{ $user->name}} {{ $user->surname}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
    @endforeach
@endforeach
@foreach ($tasks as $task)
<div class="modal fade" id="remove-task-{{ $task->id }}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <form action="{{ url("rasci/$projectId/delete-task/$task->id") }}" method="GET">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Task?</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete task ({{ $task->name }})?
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Remove</button>
                    <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>
</div>
</div>
@endforeach
@endsection
