@extends('layouts.unicornLayout')

@section('title')
  Project list
@endsection

@section('content')
<div class="col-md-12">
    @include('layouts.status')
    @include('layouts.formErrors')
    <div class="card">
      <div class="card-header">
        <div class="card-title">Project List</div>
      </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Project Name ▿</th>
                    <th scope="col">Description ▿</th>
                    <th scope="col">Cost ▿</th>
                    <th scope="col">Status ▿</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($projects as $project)
                    <tr>

                        <td>{{ $project->project_id }}</td>
                        <td>{{ $project->name }}</td>
                        <td>{{ $project->description }}</td>
                        <td>{{ $project->cost }}</td>
                        <td>{{ $project->status }}</td>
                          <td>
                            <a class="btn btn-default btn-sm"  href="{{ url("project-list/project-detail/$project->project_id") }}">
                                Detail
                            </a>
                          </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @if (Auth::user()->systemRole_id != App\User::Guest)
            <div class="card-action">
                <button class="btn btn-default" data-toggle="modal" data-target="#add-project">Add Project</button>
            </div>
            @endif
        </div>
    </div>
</div>
<div class="modal fade" id="add-project" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Project</h4>
            </div>
            <form action="{{ url('project-list/add-project') }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="project-name">Project Name*</label>
                        <input type="text" class="form-control" id="project-name" name="name" placeholder="Enter project name">
                    </div>
                    <div class="form-group">
                        <label for="comment">Project Description*</label>
                        <textarea class="form-control" id="comment" name="description" rows="1" placeholder="Enter project description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="project-manager">Project Manager</label>
                        <select  class="form-control" id="project-manager" name="project-manager">
                            @foreach ($projectManagers as $projectManager)
                                <option value={{ $projectManager->id }}> {{ $projectManager->name }} {{$projectManager->surname}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="portfolio-manager">Portfolio Manager</label>
                        <select class="form-control" id="portfolio-manager" name="portfolio-manager">
                            @foreach ($portfolioManagers as $portfolioManager)
                                <option value={{ $portfolioManager->id }}> {{ $portfolioManager->name }} {{$projectManager->surname}} </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cost">Cost*</label>
                        <input type="text" class="form-control" id="cost" name="cost" placeholder="Enter project cost">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Add</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>

    </div>
</div>
<div class="modal fade" id="remove-project" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Delete project?</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this project?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Remove</button>
                <button type="button" class="btn" data-dismiss="modal">Cancel</button>
            </div>
        </div>

    </div>
</div>

@endsection
