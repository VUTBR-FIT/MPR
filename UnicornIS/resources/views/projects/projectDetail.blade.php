@extends('layouts.unicornLayout')

@section('title')
    Project detail
@endsection

@section('content')
    <div class="container-fluid">
        @include('layouts.status')
        <h4 class="page-title"> {{ $project->name }} </h4>
        @if($project->status == 'closed')
            <h4 class="page-title"> THIS PROJECT IS CURRENTLY CLOSED </h4>
        @endif
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">About</div>
                </div>
                <div class="card-body"><p class="demo">
                        {{ $project->description }}
                    </p></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Tasks</div>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" colspan="2">Title</th>
                                <th scope="col">Responsible</th>
                                <td width="25px"></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tasks as $task)
                                <tr>
                                    <td>#{{ $task->task_id }}</td>
                                    <td colspan="2">{{ $task->name }}</td>
                                    @if (array_key_exists($task->id, $tasksData))
                                        <td>{{ $tasksData[$task->id]->name }} {{ $tasksData[$task->id]->surname }}</td>
                                    @else
                                        <td>
                                        <td>
                                    @endif
                                    <td class="remove"><a data-toggle="modal" data-target="#remove-task-{{ $task->id}}""><i
                                                class="la la-remove"></i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="card-action">
                            <button class="btn btn-default" data-toggle="modal" data-target="#add-task">Add Task
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">Active Participants</div>
                    </div>
                    <div class="card-body">
                        <div class="card-sub">
                            <b>Project Manager: </b>{{ $projectManager->name }} {{ $projectManager->surname }}
                        </div>
                        <div class="card-sub">
                            <b>Portfolio Manager: </b>{{ $portfolioManager->name }} {{ $portfolioManager->surname }}
                        </div>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Role</th>
                                <td width="25px"></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projectUsers as $u => $user)
                                <tr>
                                    <?php $role = \App\NSProle::where('id', $user->human_role)->first(); ?>
                                    <td>{{ $u+1 }}</td>
                                    <td>{{ $user->name }} {{ $user->surname }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td class="remove"><a data-toggle="modal" data-target="#remove-participant-{{ $user->human_id }}"><i
                                                    class="la la-remove"></i></a></td>
                                </tr>

                            @endforeach
                            </tbody>
                        </table>
                        <div class="card-action">
                            <button class="btn btn-default" data-toggle="modal" data-target="#add-participant">Add
                                Participant
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <a class="btn btn-danger btn-sm" href="{{ url("project-list/remove-project/$project->project_id") }}">
                Remove project
            </a>
            @if($project->status != 'closed')
                <a class="btn btn-info btn-sm" href="{{ url("project-list/close-project/$project->project_id") }}">
                    Close project
                </a>
            @endif
        </div>
    </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="add-task" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Task</h4>
                </div>
                <form name="add-task" action="{{ url("project-list/project-detail/$projectId/add-task") }}"
                      method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="task-name">Task Name</label>
                            <input type="text" class="form-control" id="task-name" name="name"
                                   placeholder="Enter task name">
                        </div>
                        <div class="form-group">
                            <label for="reponsible-people">Responsible People</label>
                            <select class="form-control" id="reponsible-people" name="person">
                                <option></option>
                                @foreach ($taskResp as $id => $value)
                                    <option value="{{ $id }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Add</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div class="modal fade" id="add-participant" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="card-title">Add Participant</h4>
                </div>
                <form action="{{ url("project-list/project-detail/$projectId/add-participant") }}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="username">User</label>
                            <select class="form-control" id="username" name="username">
                                <option></option>
                                @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }} {{ $user->surname }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role">Roles</label>
                            <select multiple="" class="form-control" id="role" name="role">
                                <option></option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Add</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @foreach ($tasks as $task)
        <div class="modal fade" id="remove-task-{{ $task->id}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{ url("rasci/$projectId/delete-task/$task->id") }}" method="GET">
                        <div class="modal-header">
                            <h4 class="modal-title">Delete Task?</h4>
                        </div>
                        <div class="modal-body">
                            Are you sure you want to delete task ({{ $task->name }})?
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Remove</button>
                            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
    @foreach($projectUsers as $user)
        <div class="modal fade" id="remove-participant-{{ $user->human_id }}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="{{ url("project-list/project-detail/$projectId/remove-participant/$user->human_id") }}" method="POST">
                        {{ csrf_field() }}
                        <div class="modal-header">
                            <h4 class="modal-title">Remove participant?</h4>
                        </div>
                        <div class="modal-body">
                            Are you sure you want to remove this participant?
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-danger">Remove</button>
                            <button type="button" class="btn" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    @endforeach

@endsection