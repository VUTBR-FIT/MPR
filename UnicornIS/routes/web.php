<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('home');
});


Route::get('home', 'HomeController@index')->name('home');

Route::get('rasci', 'RasciController@index')->name('rasci');
Route::get('rasci/{id}', 'RasciController@rasciProject')->name('rasciProject');
Route::post('rasci/{id}/add-task', 'RasciController@addTask')->name('addTask');
Route::post('rasci/{id}/update-task/{taskId}', 'RasciController@updateTask')->name('updateTask');
Route::post('rasci/{id}/update-task/{taskId}/role/{roleId}', 'RasciController@updateResponsibility')->name('updateResponsibility');
Route::get('rasci/{id}/delete-task/{taskId}', 'RasciController@removeTask')->name('removeTask');

Route::get('project-list', 'ProjectListController@index')->name('projectList');
Route::get('project-list/project-detail/{id}', 'ProjectListController@projectDetail')->name('projectDetail');
Route::post('project-list/project-detail/{id}/add-task', 'ProjectListController@addTask')->name('projectDetailAddTask');
Route::post('project-list/add-project', 'ProjectListController@addProject')->name('addProject');
Route::get('project-list/remove-project/{id}', 'ProjectListController@removeProject')->name('removeProject');
Route::get('project-list/close-project/{id}', 'ProjectListController@closeProject')->name('closeProject');
Route::post('project-list/project-detail/{id}/add-participant', 'ProjectListController@addParticipant')->name('projectDetailAddParticipant');
Route::post('project-list/project-detail/{id_project}/remove-participant/{id_participant}', 'ProjectListController@removeParticipant')->name('projectDetailRemoveParticipant');

Route::get('user-list', 'UserListController@index')->name('userList');
Route::get('user-list/add-user', 'UserListController@addUserForm')->name('addUserForm');
Route::post('user-list/add-user', 'UserListController@addUser')->name('addUser');
Route::get('user-list/edit-user/{id}', 'UserListController@editUserForm')->name('editUserForm');
Route::post('user-list/edit-user/{id}', 'UserListController@editUser')->name('editUserForm');
Route::get('user-list/remove-user/{id}', 'UserListController@removeUser')->name('removeUser');

Route::get('NSProle-list', 'NSProleListController@index')->name('roleList');
Route::get('NSProle-list/add-NSProle', 'NSProleListController@addNSProleForm')->name('addNSProleForm');
Route::post('NSProle-list/add-NSProle', 'NSProleListController@addNSProle')->name('addNSProle');
Route::get('NSProle-list/NSProle-detail/{id}', 'NSProleListController@NSProleDetail')->name('NSProleDetail');
Route::post('NSProle-list/NSProle-detail/NSProleAssignUser', 'NSProleListController@NSProleAssignUser');
Route::post('NSProle-list/NSProle-detail/removeNSProleFromUser', 'NSProleListController@removeNSProleFromUser');
Route::get('NSProle-list/edit-NSProle/{id}', 'NSProleListController@editNSProleForm')->name('editNSProleForm');
Route::post('NSProle-list/edit-NSProle/{id}', 'NSProleListController@editNSProle')->name('editNSProle');
Route::get('NSProle-list/remove-NSProle/{id}', 'NSProleListController@removeNSProle')->name('removeNSProle');

Route::get('human-resources', 'HumanResourcesController@index')->name('humanResources');
Route::get('human-resources/add-human-resource', 'HumanResourcesController@addHumanResourceForm')->name('addHumanResourceForm');
Route::post('human-resources/add-human-resource', 'HumanResourcesController@addHumanResource')->name('addHumanResource');
Route::get('human-resources/edit-human-resource/{id}', 'HumanResourcesController@editHumanResourceForm')->name('editHumanResourceForm');
Route::post('human-resources/edit-human-resource/{id}', 'HumanResourcesController@editHumanResource')->name('editHumanResource');
Route::get('human-resources/remove-human-resource/{id}', 'HumanResourcesController@removeHumanResource')->name('removeHumanResource');