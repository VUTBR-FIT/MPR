<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Lukas Cerny (xcerny63)
 */
class Responsibility extends Model {
    const TABLE_NAME = 'responsibilities';
    const COL_ID = 'id';
    const COL_CODE = 'code';
    const COL_VALUE = 'value';
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;
}
