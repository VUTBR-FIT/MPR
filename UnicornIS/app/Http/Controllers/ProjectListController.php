<?php

namespace App\Http\Controllers;

use App\HumanResource;
use App\ProjectHasHumanResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Project;
use App\User;
use App\Task;
use App\RasciCells;
use App\Responsibility;
use App\NSProle;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class ProjectListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $users = User::all();
        $projectManagers = User::where('systemRole_id', '=', User::ProjectManager)->get();
        $portfolioManagers = User::where('systemRole_id', '=', User::PortfolioManager)->get();
        return view('projects/projectList')
            ->with('projects', $projects)
            ->with('users', $users)
            ->with('projectManagers', $projectManagers)
            ->with('portfolioManagers', $portfolioManagers);
    }

    /**
     * Show detail project form.
     *
     * @return \Illuminate\Http\Response
     */
    public function projectDetail($id)
    {
        $project =  Project::where('project_id', $id)->first();
        $projectManager = User::where('id', $project->projectManager_id)->first();
        $portfolioManager = User::where('id', $project->portfolioManager_id)->first();
        $roles = NSProle::all();
        $selectResponsible = ProjectHasHumanResource::select('humanResources.name', 'humanResources.surname', ProjectHasHumanResource::TABLE_NAME . '.' . ProjectHasHumanResource::COL_ID, ProjectHasHumanResource::TABLE_NAME . '.' . ProjectHasHumanResource::COL_ROLE)
                ->join('humanResources', ProjectHasHumanResource::TABLE_NAME . '.' . ProjectHasHumanResource::COL_HR, '=', 'humanResources.id')
                ->where('project_has_human_resource.project_id', '=', $project->project_id)
                ->get();

        $selectResponsibleData = [];
        foreach ($roles as $role) {
            foreach ($selectResponsible as $row) {
                if ($row[ProjectHasHumanResource::COL_ROLE] == $role->id) {
                    $selectResponsibleData[$row[ProjectHasHumanResource::COL_ID]] = $role->name . ' (' . $row->name . ' ' . $row->surname . ')';
                }
            }
        }

        $projectUsers = ProjectHasHumanResource::join('humanResources', 'project_has_human_resource.human_id', '=', 'humanResources.id')
                ->where(ProjectHasHumanResource::TABLE_NAME.'.project_id', '=', $project->project_id)
                ->get();
        $users = DB::table('humanResources')->get();
        $tasks = Task::where(Task::COL_PROJECT, $id)->get();
        $taskData = Task::join(RasciCells::TABLE_NAME, Task::TABLE_NAME . '.' . Task::COL_ID, '=', RasciCells::TABLE_NAME . '.' . RasciCells::COL_TASK)
                ->join('humanResources', RasciCells::TABLE_NAME . '.' . RasciCells::COL_HUMAN_RES, '=', 'humanResources.id')
                ->join(Responsibility::TABLE_NAME, RasciCells::TABLE_NAME . '.' . RasciCells::COL_RESP, '=', Responsibility::TABLE_NAME . '.' . Responsibility::COL_ID)
                ->where(Task::TABLE_NAME . '.' . Task::COL_PROJECT, $id)
                ->where(Responsibility::TABLE_NAME . '.' . Responsibility::COL_CODE, 'R')->get();
        
        $data = [];
        foreach ($taskData as $row) {
            $data[$row->task_id] = $row;
        }

        return view('projects/projectDetail')
            ->with('tasks', $tasks)
            ->with('tasksData', $data)
            ->with('roles', $roles)
            ->with('users', $users)
            ->with('projectId', $project->project_id)
            ->with('project', $project)
            ->with('projectManager', $projectManager)
            ->with('portfolioManager', $portfolioManager)
            ->with('projectUsers', $projectUsers)
            ->with('taskResp', $selectResponsibleData);
    }

    /**
     * AddProject.
     *
     * @return \Illuminate\Http\Response
     */
    public function addProject(Request $request) {
        $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
          'cost' => 'required|integer'
        ]);

        $project = new Project;
        $project->name = $request->name;
        $project->description = $request->description;
        $project->status = 'in progress';
        $project->projectManager_id = $request->get('project-manager');
        $project->portfolioManager_id = $request->get('portfolio-manager');
        $project->cost = $request->cost;
        $project->save();

        return redirect('project-list')->with('statusSuccess', 'Project added');
    }

    /**
     * Remove Project
     *
     * @return \Illuminate\Http\Response
     */
    public function removeProject($id) {
        $project =  Project::where('project_id', $id)->first();
        if ($project != null) {
            $project->delete();
            return redirect('project-list')->with('statusSuccess', 'Project removed');
        }

        return redirect('project-list')->with('statusFailure', 'ID not found');
    }

    /**
     * Close Project
     *
     * @return \Illuminate\Http\Response
     */
    public function closeProject($id) {
        $project =  Project::where('project_id', $id)->first();
        if ($project != null) {
            $project->update(['status' => 'closed']);
            $project->save();
            return redirect("project-list")->with('statusSuccess', 'Project successfully closed');
        }
        return redirect('project-list')->with('statusFailure', 'ID not found');
    }

    /**
     * Add new task
     * 
     * @param integer $id       Project ID
     * @param Request $request
     */
    public function addTask($id, Request $request) {
        $project = Project::where('project_id', $id)->first();
        if ($project === null) {
            return Redirect::back()->with('statusFailure', "Project not exists.");
        }
        $this->validate($request, [
            'name' => 'required'
        ]);
        $resp = DB::table('responsibilities')
                ->where('responsibilities.value', 'Responsible')
                ->first();
        $task = new Task;
        $task->name = $request->name;
        $task[Task::COL_PROJECT] = $project->project_id;
        $task->save();
        if ($request->person != NULL) {
            $resourceProject = ProjectHasHumanResource::where(ProjectHasHumanResource::COL_ID, $request->person)->first();
            $cell = new RasciCells;
            $cell[RasciCells::COL_TASK] = $task->id;
            $cell[RasciCells::COL_ROLE] = $resourceProject->human_role;
            $cell[RasciCells::COL_HUMAN_RES] = $resourceProject->human_id;
            $cell[RasciCells::COL_RESP] = $resp->id;
            $cell->save();
        }
        return redirect()->back()->with('statusSuccess', "Task successfully added.");
    }

    public function addParticipant($id, Request $request) {
        $project = Project::where('project_id', $id)->first();
        if ($project === null) {
            return Redirect::back()->with('statusFailure', "Project not exists.");
        }
        $this->validate($request, [
            'username' => 'required',
            'role' => 'required',
        ]);

        if($phhr = ProjectHasHumanResource::where('project_id', $id)->where('human_id', $request->username)->first()) {

            $phhr->human_role = $request->role;
            $phhr->save();

            return redirect()->back()->with('statusSuccess', "Participant successfully updated.");
        } else {
            $phhr = new ProjectHasHumanResource();

            $phhr->human_id = $request->username;
            $phhr->human_role = $request->role;
            $phhr->project_id = $project->project_id;

            $phhr->save();
        }

        return redirect()->back()->with('statusSuccess', "Participant successfully added.");
    }

    public function removeParticipant($id_project, $id_participant, Request $request) {
        $phhr = ProjectHasHumanResource::where('project_id', $id_project)->where('human_id', $id_participant)->first();
        $phhr->delete();

        return redirect()->back()->with('statusSuccess', "Participant successfully deleted.");
    }

}
