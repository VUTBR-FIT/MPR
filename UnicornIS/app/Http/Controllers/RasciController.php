<?php

namespace App\Http\Controllers;

use App\NSProle;
use App\Project;
use App\Responsibility;
use App\RasciCells;
use App\ProjectHasHumanResource;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class RasciController extends Controller {

    /**
     * Create a new controller instance.
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return Redirect::back()->with('statusFailure', "Please select the project.");
    }

    /**
     * Display RASCI matrix for current project
     * 
     * @param integer $id Project ID
     */
    public function rasciProject($id) {
        $project = Project::where('project_id', $id)->first();
        if ($project === null)
            return Redirect::back()->with('statusFailure', "Project not exists.");
        $tasks = Task::where(Task::COL_PROJECT, $project->project_id)->get();
        $roles = NSProle::select('NSProles.*')->distinct('NSProles')
                        ->join(ProjectHasHumanResource::TABLE_NAME, 'NSProles.id', '=', ProjectHasHumanResource::TABLE_NAME . '.' . ProjectHasHumanResource::COL_ROLE)
                        ->where('project_id', $project->project_id)->get();//->toSql();
        //dd($roles);
        $resp = Responsibility::all();
        $users = ProjectHasHumanResource::select('humanResources.*', ProjectHasHumanResource::TABLE_NAME . '.' . ProjectHasHumanResource::COL_ROLE)
                ->join('humanResources', ProjectHasHumanResource::TABLE_NAME . '.' . ProjectHasHumanResource::COL_HR, '=', 'humanResources.id')
                ->where('project_has_human_resource.project_id', '=', $project->project_id)
                ->get();
        $cells = RasciCells::select(
                        RasciCells::TABLE_NAME . '.*', 'responsibilities.value'
                )
                ->join(Task::TABLE_NAME, RasciCells::TABLE_NAME . '.' . RasciCells::COL_TASK, '=', Task::TABLE_NAME . '.' . Task::COL_ID)
                ->join('responsibilities', RasciCells::TABLE_NAME . '.' . RasciCells::COL_RESP, '=', 'responsibilities.id')
                ->get();
        $data = [];
        foreach ($tasks as $task) {
            $row = [];
            foreach ($roles as $role) {
                foreach ($cells as $key => $cell) {
                    if ($cell->task_id == $task->id && $cell->NSP_id == $role->id) {
                        $row[$role->name] = $cell;
                        unset($cells[$key]);
                        break;
                    }
                }
            }
            $data[] = $row;
        }
        $resourcesData = [];
        foreach ($roles as $role) {
            $temp = [];
            foreach ($users as $key => $value) {
                if ($role->id == $value->human_role)
                    $temp[] = $value;
            }
            $resourcesData[$role->id] = $temp;
        }
        return view('rasci')
                        ->with('users', $resourcesData)
                        ->with('projectId', $project->project_id)
                        ->with('project', $project)
                        ->with('responsibility', $resp)
                        ->with('cells', $data)
                        ->with('roles', $roles)
                        ->with('tasks', $tasks);
    }

    /**
     * Add new task to database
     * 
     * @param integer $id Project ID
     * @param Request $request 
     */
    public function addTask($id, Request $request) {
        $project = Project::where('project_id', $id)->first();
        if ($project === null) {
            return Redirect::back()->with('statusFailure', "Project not exists.");
        }
        $this->validate($request, [
            'name' => 'required'
        ]);
        $resp = DB::table('responsibilities')
                ->where('responsibilities.value', 'Responsible')
                ->first();
        DB::beginTransaction();
        $task = new Task;
        $task->name = $request->name;
        $task[Task::COL_PROJECT] = $project->project_id;
        $task->save();
        $duplicityR = 0;
        foreach ($request->roles as $nspRoleId => $responsibilytId) {
            if ($responsibilytId !== null) {
                if ($resp->id == $responsibilytId) {
                    $duplicityR++;
                    if ($duplicityR > 1) {
                        DB::rollback();
                        break;
                    }
                }
                $cell = new RasciCells;
                $cell[RasciCells::COL_TASK] = $task->id;
                $cell[RasciCells::COL_ROLE] = $nspRoleId;
                $cell[RasciCells::COL_RESP] = $responsibilytId;
                $cell->save();
            }
        }
        if ($duplicityR < 2)
            DB::commit();
        else
            return redirect()->action('RasciController@rasciProject', [$id])->with('statusFailure', "Duplicity Responsible in one task.");
        return redirect()->action('RasciController@rasciProject', [$id])->with('statusSuccess', "Task successfully added.");
    }

    /**
     * Remove project
     * 
     * @param integer $id       Project ID
     * @param integer $taskId   Task ID
     */
    public function removeTask($id, $taskId) {
        $task = Task::where(Task::COL_ID, $taskId)->where(Task::COL_PROJECT, $id)->first();
        if ($task !== null) {
            $task->delete();
            return redirect()->back()->with('statusSuccess', "Task removed.");
        } else {
            return redirect()->back()->with('statusFailure', "Task not found.");
        }
    }

    /**
     * Update task
     * 
     * @param type $id          Project ID
     * @param type $taskId      Task ID
     * @param Request $request 
     */
    public function updateTask($id, $taskId, Request $request) {
        $task = Task::where(Task::COL_ID, $taskId)->where(Task::COL_PROJECT, $id)->first();
        if ($task !== null) {
            $cells = RasciCells::where(RasciCells::TABLE_NAME . '.' . RasciCells::COL_TASK, '=', $task[Task::COL_ID])->get();
            $responsibility = DB::table('responsibilities')
                    ->where('responsibilities.value', 'Responsible')
                    ->first();
            $duplicityR = 0;
            DB::beginTransaction();
            
            foreach ($request->roles as $roleId => $respId) {
                $founded = false;
                foreach ($cells as $key => $cell) {
                    if ($cell[RasciCells::COL_ROLE] == $roleId) {
                        if ($respId == $responsibility->id) {
                            $duplicityR++;
                            if ($duplicityR > 1) {
                                DB::rollback();
                                break;
                            }
                        }

                        if ($cell[RasciCells::COL_RESP] != $respId) {
                            if ($respId == NULL) { // If cell exists, but current options is NULL
                                $cell->delete();
                            } else {
                                RasciCells::where(RasciCells::COL_ID, '=', $cell[RasciCells::COL_ID])
                                        ->update([
                                            RasciCells::COL_RESP => $respId,
                                            RasciCells::COL_HUMAN_RES => NULL
                                ]);
                            }
                        }
                        unset($cells[$key]);
                        $founded = true;
                        break;
                    }
                }

                if (!$founded && $respId != NULL) { // Cell not exists
                    if ($respId == $responsibility->id) {
                        $duplicityR++;
                        if ($duplicityR > 1) {
                            DB::rollback();
                            break;
                        }
                    }
                    $cell = new RasciCells;
                    $cell[RasciCells::COL_TASK] = $task->id;
                    $cell[RasciCells::COL_ROLE] = $roleId;
                    $cell[RasciCells::COL_RESP] = $respId;
                    $cell->save();
                }
            }
            Task::where(Task::COL_ID, $taskId)
                    ->where(Task::COL_PROJECT, $id)
                    ->update([Task::COL_NAME => $request->name]);

            if ($duplicityR < 2)
                DB::commit();
            else
                return redirect()->action('RasciController@rasciProject', [$id])->with('statusFailure', "Duplicity Responsible in one task.");

            return redirect()->action('RasciController@rasciProject', [$id])
                            ->with('statusSuccess', 'Task updated');
        }

        return redirect()->action('RasciController@rasciProject', [$id])
                        ->with('statusFailure', 'Task not founded');
    }

    /**
     * Change responsibility in RASCI matrix (4D)
     * 
     * @param integer $id          Project ID
     * @param integer $taskId      Task ID
     * @param integer $roleId      Role ID
     * @param Request $request
     */
    public function updateResponsibility($id, $taskId, $roleId, Request $request) {
        $cell = RasciCells::select(RasciCells::TABLE_NAME . '.*')
                        ->join(Task::TABLE_NAME, RasciCells::TABLE_NAME . '.' . RasciCells::COL_TASK, '=', Task::TABLE_NAME . '.' . Task::COL_ID)
                        ->where(Task::COL_PROJECT, $id)
                        ->where(RasciCells::COL_TASK, $taskId)
                        ->where(RasciCells::COL_ROLE, $roleId)->first();
        if ($cell != NULL) {
            $cell = RasciCells::select(RasciCells::TABLE_NAME . '.*')
                    ->join(Task::TABLE_NAME, RasciCells::TABLE_NAME . '.' . RasciCells::COL_TASK, '=', Task::TABLE_NAME . '.' . Task::COL_ID)
                    ->where(Task::COL_PROJECT, $id)
                    ->where(RasciCells::COL_TASK, $taskId)
                    ->where(RasciCells::COL_ROLE, $roleId)
                    ->update([RasciCells::COL_HUMAN_RES => $request->person]);

            return redirect()->action('RasciController@rasciProject', [$id])
                            ->with('statusSuccess', 'Cell updated');
        }

        return redirect()->action('RasciController@rasciProject', [$id])
                        ->with('statusFailure', 'Cell not founded');
    }

}
