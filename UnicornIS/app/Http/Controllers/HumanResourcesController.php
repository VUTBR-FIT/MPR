<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HumanResource;

class HumanResourcesController extends Controller
{
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    	$humanResources = HumanResource::all();
        return view('humanResources/humanResourcesList')
            ->with('humanResources', $humanResources);
    }

    /**
     * Show add human resource form.
     *
     * @return \Illuminate\Http\Response
     */
    public function addHumanResourceForm() {
        return view('humanResources/addHumanResource');
    }

    /**
     * Add user.
     *
     * @return \Illuminate\Http\Response
     */
    public function addHumanResource(Request $request) {
        $this->validate($request, [
          'name' => 'required',
          'surname' => 'required',
          'salary' => 'required|integer',
          'tel' => 'sometimes|nullable|digits:10',
          'age' => 'sometimes|integer'
        ]);

        $humanResource = new HumanResource;
        $humanResource->name = $request->name;
        $humanResource->surname = $request->surname;
        $humanResource->age = $request->age;
        $humanResource->tel = $request->tel;
        $humanResource->salary = $request->salary;
        $humanResource->save();

        return redirect('human-resources')->with('statusSuccess', 'Human resource created');
    }

    /**
     * Show edit human resource form.
     *
     * @return \Illuminate\Http\Response
     */
    public function editHumanResourceForm($id)
    {
        $humanResource =  HumanResource::find($id);

        return view('humanResources/editHumanResource')
          ->with('humanResource', $humanResource);
    }

    /**
     * Show edit user form.
     *
     * @return \Illuminate\Http\Response
     */
    public function editHumanResource(Request $request, $id)

    {
        $this->validate($request, [
          'name' => 'required',
          'surname' => 'required',
          'salary' => 'required|integer',
          'tel' => 'digits:10'
        ]);

        $humanResource =  humanResource::find($id);
        $humanResource->name = $request->name;
        $humanResource->surname = $request->surname;
        $humanResource->age = $request->age;
        $humanResource->tel = $request->tel;
        $humanResource->salary = $request->salary;
        $humanResource->save();

        return redirect('human-resources')->with('statusSuccess', 'Human resource updated');
    }

    /**
     * Delete user.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeHumanResource($id)
    {
        $humanResource =  HumanResource::find($id);
        $humanResource->delete();

        return redirect('human-resources')->with('statusSuccess', 'Human Resource removed');

    }
}
