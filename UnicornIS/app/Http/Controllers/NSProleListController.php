<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NSProle;
use App\User;
use DB;

class NSPRoleListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $NSProles = NSProle::all();
        return view('NSProleList/NSProleList')
            ->with('NSProles', $NSProles);
    }

    /**
     * Show edit NSProle form.
     *
     * @return \Illuminate\Http\Response
     */
    public function editNSProleForm($id)
    {
        $NSProle =  NSProle::find($id);

        return view('NSProleList/editNSProle')
          ->with('NSProle', $NSProle);
    }

    /**
     * Show detail NSProle form.
     *
     * @return \Illuminate\Http\Response
     */
    public function NSProleDetail($id)
    {
        $NSProle =  NSProle::find($id);
        $users = User::all();
        $assignedUsersIDs = \App\HumanResource::select('humanResources.*')->distinct('humanResources')
                ->join(\App\ProjectHasHumanResource::TABLE_NAME,'humanResources.id', '=', \App\ProjectHasHumanResource::TABLE_NAME . '.' . \App\ProjectHasHumanResource::COL_HR)
                ->where(\App\ProjectHasHumanResource::TABLE_NAME . '.' . \App\ProjectHasHumanResource::COL_ROLE, '=', $NSProle->id)
                ->pluck('humanResources.id')->toArray();
        $assignedUsers = \App\HumanResource::findMany($assignedUsersIDs);


        return view('NSProleList/NSProleDetail')
          ->with('NSProle', $NSProle)
          ->with('users', $users)
          ->with('assignedUsersIDs', $assignedUsersIDs)
          ->with('assignedUsers', $assignedUsers);
    }

    /**
     * Show edit NSProle form.
     *
     * @return \Illuminate\Http\Response
     */
    public function editNSProle(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
          'salary' => 'integer'
        ]);

        $NSProle =  NSProle::find($id);
        $NSProle->name = $request->name;
        $NSProle->description = $request->description;
        $NSProle->qualification = $request->qualification;
        $NSProle->salary = $request->salary;
        $NSProle->save();

        return redirect('NSProle-list')->with('statusSuccess', 'NSP Role updated');
    }

    /**
     * Delete NSProle.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeNSProle($id)
    {
        $NSProle =  NSProle::where('id', $id)->first();
        if ($NSProle != null) {
            $NSProle->delete();
            return redirect('NSProle-list')->with('statusSuccess', 'NSP Role removed');
        }

        return redirect('NSProle-list')->with('statusFailure', 'ID not found');
    }

    /**
     * Show add NSProle form.
     *
     * @return \Illuminate\Http\Response
     */
    public function addNSProleForm()
    {
        return view('NSProleList/addNSProle');
    }

    /**
     * Add NSProle.
     *
     * @return \Illuminate\Http\Response
     */
    public function addNSProle(Request $request)
    {
        $this->validate($request, [
          'name' => 'required',
          'description' => 'required',
          'salary' => 'required|integer'
        ]);

        $NSProle = new NSProle;
        $NSProle->name = $request->name;
        $NSProle->description = $request->description;
        $NSProle->salary = $request->salary;
        $NSProle->qualification = $request->qualification;
        $NSProle->save();

        $NSProles = NSProle::all();

        return redirect('NSProle-list')->with('statusSuccess', 'NSP Role added');

    }

    /**
     * NSProleAssignUser.
     *
     * @return \Illuminate\Http\Response
     */
    public function NSProleAssignUser(Request $request) {
        // @FIXME bad table name
        DB::table('userHasNSProle')->insert([
            'user_id' => $request->get('username'),
            'NSProle_id' => $request->NSProleId
        ]);
        return redirect("NSProle-list/NSProle-detail/{$request->NSProleId}")->with('statusSuccess', 'User assigned succesfully');
    }

    /**
     * removeNSProleFromUser.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeNSProleFromUser(Request $request) {
        // @FIXME bad table name
        DB::table('userHasNSProle')->where([
            ['user_id', '=', $request->removeUser_id],
            ['NSProle_id', '=', $request->NSProleId],
        ])->delete();
        return redirect("NSProle-list/NSProle-detail/{$request->NSProleId}")->with('statusSuccess', 'User deleted from NSP role succesfully');       
    }
}
