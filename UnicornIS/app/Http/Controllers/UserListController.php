<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\SystemRole;

class UserListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show main user page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('userList/userList')
          ->with('users', $users);
    }

    /**
     * Show add user form.
     *
     * @return \Illuminate\Http\Response
     */
    public function addUserForm()
    {
        $roles = SystemRole::all();

        return view('userList/addUser')
          ->with('roles', $roles);
    }

    /**
     * Add user.
     *
     * @return \Illuminate\Http\Response
     */
    public function addUser(Request $request)
    {
        $this->validate($request, [
          'name' => 'required',
          'email' => 'required|unique:users|email',
          'password' => 'required|min:6'
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->systemRole_id = $request->role;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        $users = User::all();

        return redirect('user-list')->with('statusSuccess', 'User created');

    }

    /**
     * Show edit user form.
     *
     * @return \Illuminate\Http\Response
     */
    public function editUserForm($id)
    {
        $user =  User::find($id);
        $roles = SystemRole::all();

        return view('userList/editUser')
          ->with('user', $user)
          ->with('roles', $roles);

    }

    /**
     * Show edit user form.
     *
     * @return \Illuminate\Http\Response
     */
    public function editUser(Request $request, $id)
    {
        $this->validate($request, [
          'name' => 'required'
        ]);

        $user =  User::find($id);
        $user->name = $request->name;
        $user->surname = $request->surname;
        if ($request->role != NULL)
            $user->systemRole_id = $request->role;
        $user->save();

        return redirect('user-list')->with('statusSuccess', 'User updated');

    }

    /**
     * Delete user.
     *
     * @return \Illuminate\Http\Response
     */
    public function removeUser($id)
    {
        $user =  User::find($id);
        $user->delete();

        return redirect('user-list')->with('statusSuccess', 'User removed');

    }
}
