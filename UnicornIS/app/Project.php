<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The table primary key
     *
     * @var string
     */
    protected $primaryKey = 'project_id';

    /**
     * The table fillable
     *
     * @var string
     */
    protected $fillable = ['status'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
