<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectHasHumanResource extends Model
{
    const TABLE_NAME = 'project_has_human_resource';
    const COL_ID = 'id';
    const COL_ROLE = 'human_role';
    const COL_HR = 'human_id';

    protected $table = self::TABLE_NAME;

    public $timestamps = false;
}
