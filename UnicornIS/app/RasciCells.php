<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * @author Lukas Cerny (xcerny63)
 */
class RasciCells extends Model {
    const TABLE_NAME = 'rasciCells';
    const COL_ID = 'id';
    const COL_TASK = 'task_id';
    const COL_ROLE = 'NSP_id';
    const COL_RESP = 'responsibility_Id';
    const COL_HUMAN_RES = 'humanResource_id';
    
    protected $table = self::TABLE_NAME;
    
    protected $fillable = [self::COL_RESP];
    
    public $timestamps = false;
    
    
}
