<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HumanResource extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'humanResources';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
