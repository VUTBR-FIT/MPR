<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class SystemRole extends Model
{

    protected $table = 'systemRoles';

    use Notifiable;

}
