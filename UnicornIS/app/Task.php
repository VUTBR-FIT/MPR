<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Lukas Cerny (xcerny63)
 */
class Task extends Model {

    const TABLE_NAME = 'tasks';
    const COL_ID = 'id';
    const COL_PROJECT = 'project_id';
    const COL_NAME = 'name';

    protected $table = self::TABLE_NAME;
    
    public $timestamps = false;

}
