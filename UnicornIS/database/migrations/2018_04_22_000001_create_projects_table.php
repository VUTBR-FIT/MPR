<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'projects';

    /**
     * Run the migrations.
     * @table projects
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('project_id');
            $table->string('name', 45)->nullable();
            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();
            $table->string('description')->nullable();
            $table->integer('cost');
            $table->enum('status', ['in progress', 'closed']);
            $table->integer('projectManager_id')->unsigned()->nullable();
            $table->integer('portfolioManager_id')->unsigned()->nullable();

            $table->index(["projectManager_id"], 'fk_projects_projectManager_id_idx');
            $table->index(["portfolioManager_id"], 'fk_projects_portfolioManager_id_idx');

            $table->foreign('projectManager_id', 'fk_projects_projectManager_id_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('portfolioManager_id', 'fk_projects_portfolioManager_id_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });

        DB::table('projects')->insert(
            array(
              'project_id' => '1',
              'name' => "Testovaci projekt",
              'startDate' => null,
              'endDate' => null,
              'description' => "description",
              'cost' => 99999,
              'status' => 'in progress',
              'projectManager_id' => 4,
              'portfolioManager_id' => 3,
            )
          );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
