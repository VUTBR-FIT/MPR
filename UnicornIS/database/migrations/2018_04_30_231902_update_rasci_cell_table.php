<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRasciCellTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'rasciCells';

    /**
     * Run the migrations.
     */
    public function up() {
        if (!Schema::hasTable($this->set_schema_table))
            return;
        Schema::table($this->set_schema_table, function (Blueprint $table) {
            $table->dropForeign('fk_Task_has_NSP_role_Task1_idx');
            $table->dropColumn('task_id');
            $table->dropColumn('project_id');
            $table->dropForeign('fk_Task_has_NSP_role_Human_resource1_idx');
            $table->dropIndex('fk_Task_has_NSP_role_Human_resource1_idx');
        });
        Schema::table($this->set_schema_table, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id')->unsigned();
            $table->integer('humanResource_id')
                    ->unsigned()
                    ->nullable()
                    ->change();
            
            $table->index(["task_id"], 'fk_Task_has_NSP_role_Task1_idx');
            $table->index(["humanResource_id"], 'fk_Task_has_NSP_role_Human_resource1_idx');
            
            $table->foreign('task_id', 'fk_Task_has_NSP_role_Task1_idx')
                ->references('id')->on('tasks')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->foreign('humanResource_id', 'fk_Task_has_NSP_role_Human_resource1_idx')
                ->references('id')->on('humanResources')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down() {
        if (!Schema::hasTable($this->set_schema_table))
            return;
        Schema::table($this->set_schema_table, function (Blueprint $table) {
            $table->dropColumn('id');
            $table->integer('project_id')->unsigned();
            $table->dropForeign('fk_Task_has_NSP_role_Human_resource1_idx');
            $table->dropIndex('fk_Task_has_NSP_role_Human_resource1_idx');
        });
        Schema::table($this->set_schema_table, function (Blueprint $table) {
            $table->increments('rask_id');
            $table->integer('humanResource_id')
                    ->unsigned()
                    ->change();
            $table->index(["humanResource_id"], 'fk_Task_has_NSP_role_Human_resource1_idx');
            $table->foreign('humanResource_id', 'fk_Task_has_NSP_role_Human_resource1_idx')
                ->references('id')->on('humanResources')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

}
