<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSomeTables extends Migration
{
    public $set_schema_table = 'tasks';
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropForeign('fkkkkk');
            $table->foreign('project_id', 'fkkkkk')
                ->references('project_id')->on('projects')
                    ->onDelete('cascade');
        });
        Schema::table('userHasNSProle', function (Blueprint $table) {
            $table->dropForeign('fk_User_has_NSProle_NSProle1_idx');
            $table->foreign('NSProle_id', 'fk_User_has_NSProle_NSProle1_idx')
                ->references('id')->on('NSProles')
                ->onDelete('cascade');
        });
        Schema::table('rasciCells', function (Blueprint $table) {
            $table->dropForeign('fk_Task_has_NSP_role_NSP_role1_idx');
            $table->foreign('NSP_id', 'fk_Task_has_NSP_role_NSP_role1_idx')
                ->references('id')->on('NSProles')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropForeign('fkkkkk');
            $table->foreign('project_id', 'fkkkkk')
                ->references('project_id')->on('projects');
        });
        Schema::table('userHasNSProle', function (Blueprint $table) {
            $table->dropForeign('fk_User_has_NSProle_NSProle1_idx');
            $table->foreign('NSProle_id', 'fk_User_has_NSProle_NSProle1_idx')
                ->references('id')->on('NSProles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
        Schema::table('rasciCells', function (Blueprint $table) {
            $table->dropForeign('fk_Task_has_NSP_role_NSP_role1_idx');
            $table->foreign('NSP_id', 'fk_Task_has_NSP_role_NSP_role1_idx')
                ->references('id')->on('NSProles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }
}
