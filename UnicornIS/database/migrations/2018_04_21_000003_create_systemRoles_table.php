<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemrolesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'systemRoles';

    /**
     * Run the migrations.
     * @table systemRoles
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
        });

        // Insert some stuff
        DB::table('systemRoles')->insert(
            array(
              'id' => '1',
              'name' => "admin",
              'description' => "admin"
            )
          );
          DB::table('systemRoles')->insert(
              array(
                'id' => '2',
                'name' => "CEO",
                'description' => "CEO"
              )
            );
            DB::table('systemRoles')->insert(
                array(
                  'id' => '3',
                  'name' => "Portfolio manager",
                  'description' => "Portfolio manager"
                )
              );
              DB::table('systemRoles')->insert(
                  array(
                    'id' => '4',
                    'name' => "Project manager",
                    'description' => "Project manager"
                  )
                );
                DB::table('systemRoles')->insert(
                    array(
                      'id' => '5',
                      'name' => "Guest",
                      'description' => "Guest"
                    )
                  );
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
