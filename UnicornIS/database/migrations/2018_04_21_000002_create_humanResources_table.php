<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHumanresourcesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'humanResources';

    /**
     * Run the migrations.
     * @table humanResources
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('surname', 45)->nullable();
            $table->integer('age')->nullable();
            $table->integer('tel')->nullable();
            $table->integer('salary')->nullable();
        });

        DB::table('humanResources')->insert(
            array(
              'id' => '1',
              'name' => "Adam",
              'surname' => "Bezak",
              'age' => "22",
              'salary' => '25000'
            )
        );

        DB::table('humanResources')->insert(
            array(
              'id' => '2',
              'name' => "Dominika",
              'surname' => "Klobucnikova",
              'salary' => '300000'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
