<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserhasprojectTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'userHasProject';

    /**
     * Run the migrations.
     * @table userHasProject
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned();
            $table->integer('project_id')->unsigned();

            $table->index(["project_id"], 'fk_User_has_Project_Project1_idx');

            $table->index(["user_id"], 'fk_User_has_Project_User1_idx');


            $table->foreign('user_id', 'fk_User_has_Project_User1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('project_id', 'fk_User_has_Project_Project1_idx')
                ->references('project_id')->on('projects')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
