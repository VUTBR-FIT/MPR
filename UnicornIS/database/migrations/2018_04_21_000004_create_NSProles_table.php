<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNsprolesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'NSProles';

    /**
     * Run the migrations.
     * @table NSProles
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 200)->nullable();
            $table->string('description', 1000)->nullable();
            $table->string('qualification', 45)->nullable();
            $table->integer('salary')->nullable();
        });

        DB::table('NSProles')->insert(
            array(
              'id' => '1',
              'name' => "PROGRAMÁTOR ANALYTIK",
              'description' => "Programátor analytik na základě systémové analýzy procesů a požadavků uživatelů vytváří logické a strukturální diagramy informačních systémů a počítačových aplikací, programuje, testuje a ladí počítačové programy.",
              'qualification' => "",
              'salary' => '35000',
            )
        );

        DB::table('NSProles')->insert(
            array(
              'id' => '2',
              'name' => "SPECIALISTA IT",
              'description' => "Specialista informačních technologií komplexně stanovuje směry technického rozvoje podle podmínek programového a systémového vybavení a s ohledem na potřeby organizace.",
              'qualification' => "",
              'salary' => '30000',
            )
        );

        DB::table('NSProles')->insert(
            array(
              'id' => '3',
              'name' => "SPRÁVCE DATOVÉHO CENTRA",
              'description' => "Správce datového centra provádí obsluhu kritických systémů datového centra od rutinních konfiguračních úkonů a monitorování síťového a datového provozu, až po operativní změny v nastavení infrastrukturních zdrojů pro zajištění nepřetržitého chodu (tzv. Vysoké dostupnosti/High Availability) datového centra.",
              'qualification' => "",
              'salary' => '30000',
            )
        );

        DB::table('NSProles')->insert(
            array(
              'id' => '4',
              'name' => "SPECIALISTA OPERAČNÍCH SYSTÉMŮ A SÍTÍ",
              'description' => "Specialista operačních systémů a sítí komplexně ověřuje, oživuje a nastavuje parametry operačních systémů počítačů a počítačových sítí.",
              'qualification' => "",
              'salary' => '30000',
            )
        );

        DB::table('NSProles')->insert(
            array(
              'id' => '5',
              'name' => "GRAFIK UŽIVATELSKÉHO ROZHRANÍ",
              'description' => "Grafik uživatelského rozhraní navrhuje vzhled a uspořádání uživatelských rozhraní počítačových aplikací.",
              'qualification' => "",
              'salary' => '30000',
            )
        );

        DB::table('NSProles')->insert(
            array(
              'id' => '6',
              'name' => "TESTER",
              'description' => "Tester automatizovaného testování vyvíjí a udržuje softwarové testy, spouští tyto testy a zpracovává výsledky jejich běhu.",
              'qualification' => "",
              'salary' => '25000',
            )
        );

        DB::table('NSProles')->insert(
            array(
              'id' => '7',
              'name' => "WEBDESIGNER",
              'description' => "Webdesigner vytváří webové stránky na základě zadání od klienta a potřeb budoucích návštěvníků webu, zpracovává návrh obsahu a struktury webu, vytváří grafické návrhy webových stránek a kóduje šablony jednotlivých webových stránek, které otestuje a implementuje do systému pro správu obsahu CMS (Content Management System).",
              'qualification' => "",
              'salary' => '20000',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
