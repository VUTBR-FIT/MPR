<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectHasHumanResourceTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'project_has_human_resource';

    /**
     * Run the migrations.
     * @table project_has_human_resource
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('project_id');
            $table->integer('human_id')->unsigned();

            $table->index(["human_id"], 'fk_Project_has_Human_resource_Human_resource1_idx');

            $table->index(["project_id"], 'fk_Project_has_Human_resource_Project1_idx');


            $table->foreign('project_id', 'fk_Project_has_Human_resource_Project1_idx')
                ->references('project_id')->on('projects')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('human_id', 'fk_Project_has_Human_resource_Human_resource1_idx')
                ->references('id')->on('humanResources')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
