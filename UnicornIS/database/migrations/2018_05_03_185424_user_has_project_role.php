<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserHasProjectRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_has_human_resource', function (Blueprint $table) {
            $table->integer('human_role')->unsigned();
            $table->foreign('human_role', 'fk_project_human_resources_role_id')
                ->references('id')->on('NSProles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_has_human_resource', function (Blueprint $table) {
            $table->dropForeign('fk_project_human_resources_role_id');
            $table->dropColumn('human_role');
        });
    }
}
