<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

            // $table->integer('systemRole_id');
            $table->string('surname', 45)->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();

            $table->integer('systemRole_id')->unsigned()->default(1);

            $table->index(["systemRole_id"], 'fk_User_System_role_idx');

            $table->foreign('systemRole_id', 'fk_User_System_role_idx')
                ->references('id')->on('systemRoles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });

        DB::table('users')->insert(
            array(
              'id' => '1',
              'name' => "admin",
              'surname' => "admin",
              'email' => "admin@unicorn.com",
              'password' => bcrypt('123456'),
              'systemRole_id' => '1'
            )
          );
        DB::table('users')->insert(
            array(
              'id' => '2',
              'name' => "Ondrej",
              'surname' => "Kopal",
              'email' => "kopal@unicorn.com",
              'password' => bcrypt('123456'),
              'systemRole_id' => '2'
            )
          );
          DB::table('users')->insert(
              array(
                'id' => '3',
                'name' => "Tomáš",
                'surname' => "Horký",
                'email' => "horky@unicorn.com",
                'password' => bcrypt('123456'),
                'systemRole_id' => '3'
              )
            );
          DB::table('users')->insert(
              array(
                'id' => '4',
                'name' => "Adam",
                'surname' => "Bezák",
                'email' => "bezak@unicorn.com",
                'password' => bcrypt('123456'),
                'systemRole_id' => '4'
              )
            );
          DB::table('users')->insert(
              array(
                'id' => '5',
                'name' => "Mirek",
                'surname' => "Novák",
                'email' => "novak@unicorn.com",
                'password' => bcrypt('123456'),
                'systemRole_id' => '5'
              )
            );
          DB::table('users')->insert(
              array(
                'id' => '6',
                'name' => "Radek",
                'surname' => "Doležal",
                'email' => "dolezal@unicorn.com",
                'password' => bcrypt('123456'),
                'systemRole_id' => '3'
              )
            );
          DB::table('users')->insert(
              array(
                'id' => '7',
                'name' => "Patrik",
                'surname' => "Potoček",
                'email' => "potocek@unicorn.com",
                'password' => bcrypt('123456'),
                'systemRole_id' => '4'
              )
            );
          }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
