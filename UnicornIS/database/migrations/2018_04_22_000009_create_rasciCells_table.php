<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRascicellsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'rasciCells';

    /**
     * Run the migrations.
     * @table rasciCells
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('task_id');
            $table->integer('project_id')->unsigned();
            $table->integer('NSP_id')->unsigned();
            $table->integer('humanResource_id')->unsigned();
            $table->integer('responsibility_Id')->unsigned();

            $table->index(["humanResource_id"], 'fk_Task_has_NSP_role_Human_resource1_idx');

            $table->index(["NSP_id"], 'fk_Task_has_NSP_role_NSP_role1_idx');

            $table->index(["task_id", "project_id"], 'fk_Task_has_NSP_role_Task1_idx');

            $table->index(["responsibility_Id"], 'fk_Task_has_NSP_role_Responsibility1_idx');


            $table->foreign('task_id', 'fk_Task_has_NSP_role_Task1_idx')
                ->references('id')->on('tasks')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('NSP_id', 'fk_Task_has_NSP_role_NSP_role1_idx')
                ->references('id')->on('NSProles')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('humanResource_id', 'fk_Task_has_NSP_role_Human_resource1_idx')
                ->references('id')->on('humanResources')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('responsibility_Id', 'fk_Task_has_NSP_role_Responsibility1_idx')
                ->references('id')->on('responsibilities')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
