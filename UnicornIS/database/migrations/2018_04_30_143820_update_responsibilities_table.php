<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateResponsibilitiesTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'responsibilities';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table($this->set_schema_table, function (Blueprint $table) {
            $table->string('code', 2);
        });

        DB::table($this->set_schema_table)->insert([
            [
                'id' => 1,
                'value' => "Responsible",
                'code' => "R",
            ],
            [
                'id' => 2,
                'value' => "Accountable",
                'code' => "A",
            ],
            [
                'id' => 3,
                'value' => "Support",
                'code' => "S",
            ],
            [
                'id' => 4,
                'value' => "Consulted",
                'code' => "C",
            ],
            [
                'id' => 5,
                'value' => "Informed",
                'code' => "I",
            ],
                ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        if (!Schema::hasTable($this->set_schema_table))
            return;
        Schema::table($this->set_schema_table, function (Blueprint $table) {
            $table->dropColumn('code');
        });
    }

}
