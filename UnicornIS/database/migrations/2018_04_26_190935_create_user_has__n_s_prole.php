<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHasNSProle extends Migration
{

    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'userHasNSProle';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('user_id')->unsigned();
            $table->integer('NSProle_id')->unsigned();

            $table->index(["user_id"], 'fk_User_has_NSProle_User2_idx');
            $table->index(["NSProle_id"], 'fk_User_has_NSProle_NSProle1_idx');

            $table->foreign('user_id', 'fk_User_has_NSProle_User2_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('NSProle_id', 'fk_User_has_NSProle_NSProle1_idx')
                ->references('id')->on('NSProles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });

        DB::table('userHasNSProle')->insert(
            array(
              'user_id' => '1',
              'NSProle_id' => "1",
            )
        );
        DB::table('userHasNSProle')->insert(
            array(
              'user_id' => '2',
              'NSProle_id' => "1",
            )
        );
        DB::table('userHasNSProle')->insert(
            array(
              'user_id' => '3',
              'NSProle_id' => "1",
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
